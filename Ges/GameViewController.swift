//
//  GameViewController.swift
//  Ges
//
//  Created by Di Monda Davide on 01/02/2019.
//  Copyright © 2019 Raigeki. All rights reserved.
//

import Foundation
import UIKit

class GameViewController : UIViewController{
    
    @IBOutlet weak var rectangleCmd: UIImageView!  //rectangle used for the control panel
    
    @IBOutlet weak var eventButton: UIButton!
    @IBOutlet weak var viewCmd: UIView!   //view that contains the control panel
    @IBOutlet weak var refuseButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    
    @IBOutlet var starsDelayNull: [UIImageView]!  //array that contains all the stars in background with delay of animation 0.0
    @IBOutlet var starsDelay03: [UIImageView]!
    @IBOutlet var starsDelay06: [UIImageView]!
    
//    GamePlanet Image
    @IBOutlet weak var gamePlanet: UIImageView!
    
    //MARK: Load func
    override func viewWillAppear(_ animated: Bool) {
        startPulseAnimations(withDelay : 0.0)
        startPulseAnimations(withDelay : 0.3)
        startPulseAnimations(withDelay : 0.6)
        
    //setup the gif planet
        setupPlanet()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    
    //MARK: Actions
    //Action when you tap on the event btm
    @IBAction func eventAction(_ sender: Any) {

        UIView.animate(withDuration: 1) {
            
            let moveDownCmd = CGAffineTransform(translationX: 0, y: -450)
            self.viewCmd.transform = moveDownCmd
            
        }
    }
    
    
    //Actions when decision is picked
    @IBAction func refuseEvent(_ sender: Any) {
        
        UIView.animate(withDuration: 1) {
            
            let moveUpCmd = CGAffineTransform(translationX: 0, y: 0)
            self.viewCmd.transform = moveUpCmd
            
        }
    }
    
    //Actions when decision is picked
    @IBAction func acceptEvent(_ sender: Any) {
        
        UIView.animate(withDuration: 1) {
            
            let moveUpCmd = CGAffineTransform(translationX: 0, y: 0)
            self.viewCmd.transform = moveUpCmd
            
        }
    }

    //MARK: Animations
    //Blink animation of the starts with a delay of 0.3s or 0.6s or 0.0s
    func startPulseAnimations(withDelay : Double){
        
        UIView.animateKeyframes(withDuration: 2, delay: withDelay, options: [.repeat, .autoreverse], animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.5, animations: {
                let widening = CGAffineTransform(scaleX: 1.35, y: 1.35)
                
                if(withDelay == 0.3){
                    for star in self.starsDelay03{
                        star.transform = widening
                    }
                }else if(withDelay == 0.6){
                    for star in self.starsDelay06{
                        star.transform = widening
                    }
                }else if(withDelay == 0.0){
                    for star in self.starsDelayNull{
                        star.transform = widening
                    }
                }
               
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5, animations: {
                let narrowing = CGAffineTransform(scaleX: 1, y: 1)
                
                if(withDelay == 0.3){
                    for star in self.starsDelay03{
                        star.transform = narrowing
                    }
                }else if(withDelay == 0.6){
                    for star in self.starsDelay06{
                        star.transform = narrowing
                    }
                }else if(withDelay == 0.0){
                    for star in self.starsDelayNull{
                        star.transform = narrowing
                    }
                }
            })
        })
    }
    
    
    @IBAction func backToMain(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
//    function to make the gameplanet fade-in and setting up the gif
    func setupPlanet(){
        gamePlanet.alpha = 0.0000001
        gamePlanet.loadGif(name: "GoodPlanet")
        UIView.animate(withDuration: 10, animations: {
            self.gamePlanet.alpha = 1.0
        })
    }
}
