//
//  Story.swift
//  Ges
//
//  Created by Alessandro Scala on 01/02/2019.
//  Copyright © 2019 Raigeki. All rights reserved.
//

import Foundation

//class Story{
//    var ID: Int
//    var name: String
//    var soundTrack: [String]    //Array with the music filenames
//    var events: [Event]         //All the possible events/questions
//    var endings: [Ending]       //All the possible endings
//
//    var currentEvent: Event
//    var history: [(Int, Bool)]
//
//    var delegate: StoryDelegate?
//
//    //Just an initializer created to stop the compiler from giving errors
//    init(ID: Int, name: String, soundTrack: [String], events: [Event], endings: [Ending], first: [Int]){
//        self.ID = ID
//        self.name = name
//        self.soundTrack = soundTrack
//        self.events = events
//        self.endings = endings
//
//        self.history = []
//        self.currentEvent = Event(ID: 0, text: "", answers: ("",""), eventPrerequisites: nil, resourcePrerequisites: nil, next: ([],[]), probability: [:], ending: nil, outcome: (((0,0),(0,0),(0,0)), ((0,0),(0,0),(0,0))), soundtrackChange: nil)
//        self.currentEvent = chooseEvent(between: first)
//    }
//
//    func chooseEvent(between: [Int]) -> Event{  //Sorry, this code will be unreadable
//        let probabilities:[Int: [(Int, Int)]] = {
//            var out: [Int: [(Int, Int)]] = [:]
//            for ev in between{
//                if canHappen(event: ev){
//                    let probability = getProbability(for: ev)
//                    for (phase, prob) in probability{
//                        if out[phase] == nil{
//                            out[phase] = []
//                        }
//                        out[phase]!.append((ev, prob))
//                    }
//                }else{
//                    continue
//                }
//            }
//            return out
//        }()
//
//        for (_, prob) in probabilities.sorted(by: {$0.key<$1.key}){
//            if let selected = selectOne(in: prob){
//                return getEvent(with: selected)!
//            }else{
//                continue
//            }
//        }
//
//        return Event(ID: 0, text: "", answers: ("",""), eventPrerequisites: nil, resourcePrerequisites: nil, next: ([],[]), probability: [:], ending: nil, outcome: (((0,0),(0,0),(0,0)), ((0,0),(0,0),(0,0))), soundtrackChange: nil)
//    }
//
//    private func selectOne(in probabilities: [(Int, Int)]) -> Int?{
//        var sum = 0
//        let r = Int.random(in: 0..<100)
//        for i in 0..<probabilities.count{
//            if sum..<(sum+probabilities[i].1) ~= r{
//                return probabilities[i].0
//            }
//            sum += probabilities[i].1
//        }
//        return nil
//    }
//
//    private func canHappen(event ID: Int) -> Bool{
//        if let prerequisites = getEvent(with: ID)?.eventPrerequisites{
//            for pre in prerequisites{
//                if !history.contains(where: {($0.0 == pre.0) && ($0.1 == pre.1)}){
//                    return false
//                }
//            }
//            return true
//        }else{
//            return true
//        }
//    }
//
//    private func getProbability(for event: Int) -> [Int: Int]{
//        return getEvent(with: event)!.probability
//    }
//
//    private func getEvent(with id: Int) -> Event?{
//        return events.first(where: {$0.ID == id})
//    }
//
//    func answer(_ answer: Bool) -> (Int, Int, Int){
//        let oc = answer ? self.currentEvent.outcome.0 : self.currentEvent.outcome.1
//
//        history.append((currentEvent.ID, answer))
//
//        if let end = currentEvent.ending{
//            delegate?.trigger(ending: getEnding(with: end))
//        }
//
//        currentEvent = chooseEvent(between: answer ? currentEvent.next.0 : currentEvent.next.1)
//
//        return (oc.0.0 + Int.random(in: -oc.0.1...oc.0.1), oc.1.0 + Int.random(in: -oc.1.1...oc.1.1), oc.2.0 + Int.random(in: -oc.2.1...oc.2.1))
//    }
//
//    private func getEnding(with ID: Int) -> Ending{
//        return endings.first(where: {$0.ID == ID})!
//    }
//}
//
//struct Event{
//    var ID: Int
//    var text: String    //Text displayed when the event happens
//    var answers: (String, String)   //The two possible answers
//    var eventPrerequisites: [(Int, Bool)]?    //Events you have to have answered to in a certain way for this event to happen
//    var resourcePrerequisites: (Int, Int, Int)?    //Events you have to have answered to in a certain way for this event to happen
//    var next: ([Int], [Int])        //Two arrays containing the events that can happen after this one, based on how you answered to it
//    var probability: [Int: Int]     //This one is needed to decide if the event should happen or not
//    var ending: Int?      //If this event should trigger an ending, this will be the id
//    var outcome: (((Int, Int), (Int, Int), (Int, Int)), ((Int, Int), (Int, Int), (Int, Int)))   //Don't kill me pls
//    var soundtrackChange: Int?
//}
//
//struct Ending{
//    var ID: Int
//    var filename: String    //The name of the file that contains the animation to be played
//    var soundtrack: Int
//}
//
//
//protocol StoryDelegate{
//    func trigger(ending: Ending)
//}
