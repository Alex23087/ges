//
//  OptionsViewController.swift
//  Ges
//
//  Created by Alessandro Scala on 31/01/2019.
//  Copyright © 2019 Raigeki. All rights reserved.
//

import UIKit

class OptionsViewController: UIViewController {
    
    //Creating enum for language as it is easier to deal with it
    enum Language: Int{
        case eng = 0
        case it = 1
        case 中文 = 2
    }
    //Text representation of the language to set as the button text
    var langNames: [String] = ["ENG", "ITA", "中文"]
    
    var colorBlind: Bool = true
    var lang: Language = .eng //Default language is english

    @IBOutlet weak var colorBlindBtn: UIButton!
    @IBOutlet weak var bgMusicSlider: UISlider!
    @IBOutlet weak var sfxSlider: UISlider!
    @IBOutlet weak var langBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setting display properties for the color blind checkbox
        colorBlindBtn.layer.borderWidth = 5
        colorBlindBtn.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        colorBlindBtn.layer.masksToBounds = true
        colorBlindBtn.layer.cornerRadius = 7
        colorBlindBtn.addTarget(self, action: #selector(toggleColorBlind), for: .touchDown) //Setting the 'onClick' for the checkbox
        langBtn.addTarget(self, action: #selector(switchLanguage), for: .touchDown) //Setting the 'onClick' for the language button
        
        //Setting the display properties for the back button
        backBtn.layer.borderWidth = 5
        backBtn.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        backBtn.layer.masksToBounds = true
        backBtn.layer.cornerRadius = 7
        backBtn.addTarget(self, action: #selector(backToMain), for: .touchUpInside) //Setting the 'onClick' for the back button
    }
    
    @objc func toggleColorBlind(){
        //If the color blind option is active, deactivate it, else activate it
        if(colorBlind){
            colorBlind = false
            colorBlindBtn.setTitle("", for: .normal)
        }else{
            colorBlind = true
            colorBlindBtn.setTitle("✓", for: .normal)
        }
    }
    
    @objc func switchLanguage(){
        lang = Language(rawValue: (lang.rawValue + 1) % 3)! //Change the language adding 1 and letting it wrap back using modulo 3
        langBtn.setTitle(langNames[lang.rawValue], for: .normal) //Set the text for the selected language
    }
    
    @objc func backToMain(){
        self.dismiss(animated: true, completion: nil) //Dismiss the option view controller and go back to main menu
    }

}
