//
//  MainViewController.swift
//  Ges
//
//  Created by Alessandro Scala on 31/01/2019.
//  Copyright © 2019 Raigeki. All rights reserved.
//

import UIKit
import AVKit

class MainViewController: UIViewController {
    var player: AVAudioPlayer?

    @IBOutlet weak var startBtn: UIView!
    @IBOutlet weak var shopBtn: UIView!
    @IBOutlet weak var endingsBtn: UIView!
    @IBOutlet weak var optionsBtn: UIButton!
    @IBOutlet weak var mainPlanet: UIImageView!
    @IBOutlet var starsDelayNull: [UIImageView]!
    @IBOutlet var starsDelay05: [UIImageView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startBtn.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(startClicked))) //Set the 'onClick' property of the start 'button' (it's a view, not a button)
        
        //Setting display properties for the options button
        optionsBtn.layer.borderWidth = 5
        optionsBtn.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        optionsBtn.layer.masksToBounds = true
        optionsBtn.layer.cornerRadius = 7
        optionsBtn.addTarget(self, action: #selector(optionsClicked), for: .touchUpInside) //Set the 'onClick' property for the options button
        player = try! AVAudioPlayer(data: NSDataAsset(name: "Space42")!.data)
        player?.numberOfLoops = Int.max
        player?.play()
        //Func to make the planet gif rotate (might need improvement)
        setupGif()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        startPulseAnimations(withDelay : 0.0)
        startPulseAnimations(withDelay : 0.5)
    }
    
    @objc func startClicked(){
        performSegue(withIdentifier: "ToGame", sender: self)
    }
    @objc func shopClicked(){
        performSegue(withIdentifier: "ToShop", sender: self)
    }
    @objc func endingsClicked(){
        performSegue(withIdentifier: "ToEndings", sender: self)
    }
    @objc func optionsClicked(){
        performSegue(withIdentifier: "ToOptions", sender: self)
    }
//    func to set gif movement using Swift+Gif extension
    func setupGif(){
        mainPlanet.loadGif(name: "GoodPlanet")
    }
    
    
    //Blink animation of the starts with a delay of 0.5s or 0.0s
    func startPulseAnimations(withDelay : Double){
        
        UIView.animateKeyframes(withDuration: 2, delay: withDelay, options: [.repeat, .autoreverse], animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.5, animations: {
                let widening = CGAffineTransform(scaleX: 1.35, y: 1.35)
                
                if(withDelay == 0.5){
                    for star in self.starsDelay05{
                        star.transform = widening
                    }
                }else if(withDelay == 0.0){
                    for star in self.starsDelayNull{
                        star.transform = widening
                    }
                }
                
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5, animations: {
                let narrowing = CGAffineTransform(scaleX: 1, y: 1)
                
                if(withDelay == 0.5){
                    for star in self.starsDelay05{
                        star.transform = narrowing
                    }
                }else if(withDelay == 0.0){
                    for star in self.starsDelayNull{
                        star.transform = narrowing
                    }
                }
            })
        })
    }
    
    
}
