//
//  GesTests.swift
//  GesTests
//
//  Created by Alessandro Scala on 31/01/2019.
//  Copyright © 2019 Raigeki. All rights reserved.
//

import XCTest
@testable import Ges

class GesTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testStorySystem(){
        var events: [Event] = []
        
        events.append(Event(ID: 0, text: "", answers: ("",""), prerequisites: [], next: ([],[]), probability: [0:100], endings: nil))
        events.append(Event(ID: 1, text: "", answers: ("",""), prerequisites: [], next: ([],[]), probability: [1:50], endings: nil))
        events.append(Event(ID: 2, text: "", answers: ("",""), prerequisites: [], next: ([],[]), probability: [2:50], endings: nil))
        events.append(Event(ID: 3, text: "", answers: ("",""), prerequisites: [], next: ([],[]), probability: [2:50], endings: nil))
        
        var s = Story(ID: 0, name: "s", soundTrack: [], events: events, endings: [], first: [0,1,2,3])
        
        XCTAssertTrue(s.currentEvent.ID == 0)
    }

}
